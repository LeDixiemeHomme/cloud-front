import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Item} from "../models/Item";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {

  private readonly host: string;

  constructor(private http: HttpClient) {
    this.host = environment.host;
  }

  public getItems(): Observable<Item[]>{
    return this.http.get<Item[]>(this.host + '/items');
  }

  public addItem(item: Item): Observable<Item>{
    return this.http.post<Item>(this.host + '/items', item);
  }

}
