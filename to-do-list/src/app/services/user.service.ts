import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/User";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly host: string;

  constructor(private http: HttpClient) {
    this.host = environment.host;
  }

  public getUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.host + '/users');
  }
}
