import {Item} from "./Item";

export class ToDoList {
  id: string;
  items: Item[];
}
