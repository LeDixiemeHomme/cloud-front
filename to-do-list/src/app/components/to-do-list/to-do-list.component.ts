import {Component, OnInit, ViewChild} from '@angular/core';
import {Item} from "../../models/Item";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {ToDoListService} from "../../services/to-do-list.service";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})

export class ToDoListComponent implements OnInit {

  displayedColumns: string[] = ['position', 'title', 'content', 'status', 'user'];

  dataSource = new MatTableDataSource<Item>();

  constructor(private toDoListService: ToDoListService, private router: Router) {}

  ngOnInit() {
    this.toDoListService.getItems().subscribe((data) => {
      this.dataSource.data = data
    });
  }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
